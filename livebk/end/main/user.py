__author__ = 'db'
# encoding=utf-8
import tornado.web
from pymongo import MongoClient
import settings
import datetime


class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("username")


class RegisterHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("register.html")

    def post(self, *args, **kwargs):
        username = self.get_argument("username")
        password = self.get_argument("password")
        password_again = self.get_argument("password_again")
        email = self.get_argument("email")
        agree = self.get_argument("agree", "false")

        if agree != "true":
            self.write("您尚未同意《用户协议》及《隐私政策》！")
            return

        print "agree is legal"

        if password != password_again:
            self.write("您输入的密码不一致！")
            return

        print "password is legal"

        db = MongoClient(settings.mdb_server, settings.mdb_port).livebk

        if db.user.find_one({"username": username}) is not None:
            self.write("用户名已经被注册！")
            return

        print "username is legal"

        if db.user.find_one({"email": email}) is not None:
            self.write("邮箱已经被注册！")
            return

        print "email is legal"

        object_id = db.user.insert({
            "username": username,
            "password": password,
            "email": email
        })

        if object_id is not None:
            self.write("注册成功")


class UserHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.render("user.html", username=self.current_user)


class LoginHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("login.html")

    def post(self, *args, **kwargs):
        username = self.get_argument("username")
        password = self.get_argument("password")
        next = self.get_argument("next", "/")

        print "next : " + next

        db = MongoClient(settings.mdb_server, settings.mdb_port).livebk

        if db.user.find_one({"username": username, "password": password}) is not None:
            self.set_secure_cookie("username", username)
            self.redirect(next)


class LogoutHandler(tornado.web.RequestHandler):
    def post(self):
        self.clear_cookie("username")
        self.redirect("/")


