# coding=utf-8
__author__ = 'db'

import os.path
import re
import datetime

import tornado.web
from pymongo import MongoClient
import settings


class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("username")


class LiveHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.render("live.html", username=self.get_secure_cookie("username"))

    @tornado.web.authenticated
    def post(self, *args, **kwargs):
        message = self.get_argument("message")
        anonymous = self.get_argument("anonymous", False)
        photo = self.request.files["photo"]

        if anonymous != False:
            anonymous = True

        upload_path = os.path.join(settings.BASE_DIR, "front", "static", "photos")

        timestamp = re.compile("[\.:\- ]").sub("", str(datetime.datetime.now()))
        file_name = None
        file = None

        for meta in photo:
            file_name = meta['filename']
            file = meta["body"]

        photo_name = timestamp + "_" + file_name
        file_path = os.path.join(upload_path, photo_name)

        db = MongoClient(settings.mdb_server, settings.mdb_port).livebk
        object_id = db.live.insert({
            "message": message,
            "anonymous": anonymous,
            "photo_name": photo_name,
            "username": self.get_secure_cookie("username")
        })

        if object_id is not None:
            with open(file_path, 'wb') as up:
                up.write(file)
            self.redirect("/")

