__author__ = 'db'

import os.path

import user
import index
import live

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

settings = {
    "template_path": os.path.join(BASE_DIR, "front", "templates"),
    "static_path": os.path.join(BASE_DIR, "front", "static"),
    "debug": True,
    "cookie_secret": "EOFsnUQ/SeG2V39seKNw0hwYJSevqEjxs/+lMFfHSd4=",
    "xsrf_cookies": True,
    "login_url":"/login"

}

handlers = [
    ("/", index.IndexHandler),
    ("/register", user.RegisterHandler),
    ("/user", user.UserHandler),
    ("/login", user.LoginHandler),
    ("/logout", user.LogoutHandler),
    ("/live", live.LiveHandler)
]

# template_path = os.path.join(BASE_DIR, "front", "templates")
#
# static_path = os.path.join(BASE_DIR, "front", "static")
#
# debug = True

mdb_server = "localhost"

mdb_port = 27017
