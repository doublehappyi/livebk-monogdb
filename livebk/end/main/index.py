__author__ = 'db'

import tornado.web
import pymongo
from pymongo import MongoClient
import settings


class IndexHandler(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):
        db = MongoClient(settings.mdb_server, settings.mdb_port).livebk
        lives = db.live.find().sort("photo_name",pymongo.DESCENDING)
        self.render("index.html", username=self.get_secure_cookie("username"), lives=lives)